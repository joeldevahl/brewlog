import os
basedir = os.path.abspath(os.path.dirname(__file__))
db_name = os.path.join(basedir, 'app.db')

SQLALCHEMY_DATABASE_URI = 'sqlite:///' + db_name
SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')

CSRF_ENABLED = True

SECRET_KEY = 'development key'
