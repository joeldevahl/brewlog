from flask import render_template
from flask.ext.login import login_required
from app import app
from model import Event

@app.route('/index')
@login_required
def index():
	events = Event.query.order_by(Event.id.desc()).limit(20).all()
	return render_template('index.html', events = events)
