from hashlib import sha1
from flask import redirect, url_for, request, render_template
from model import User
from flask.ext.login import login_required
from app import app, db
from forms import UserForm

@app.route('/user', methods = ['GET', 'POST'])
@app.route('/user/<int:user_id>', methods = ['GET'])
@login_required
def user(user_id = None):
	form = UserForm()

	if form.validate_on_submit():
		user = User(name = request.form['name'], username = request.form['username'], password = bytearray(sha1(request.form['password']).digest()))
		db.session.add(user)
		db.session.commit()
		return redirect(url_for('user', user_id = user.id))

	elif user_id:
		user = User.query.filter(User.id == user_id).one()
		return render_template('user.html', user = user, form = form)

	else:
		users = User.query.all()
		return render_template('user.html', users = users, user = None, form = form)

@app.route('/user/delete/<int:user_id>', methods = ['GET', 'POST'])
@login_required
def delete_user(user_id):
	user = User.query.filter(User.id == user_id).one()
	db.session.delete(user)
	db.session.commit()
	return redirect(url_for('user'))
