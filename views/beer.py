from flask import redirect, url_for, request, render_template
from model import Beer
from login import login_required
from app import app, db

@app.route('/beer/<int:beer_id>', methods = ['GET'])
@app.route('/beer', methods = ['GET', 'POST'])
@login_required
def beer(beer_id = None):
	if request.method == 'POST':
		beer = Beer(name = request.form['name'])
		db.session.add(beer)
		db.session.commit()
		return redirect(url_for('beer', beer_id = beer.id))

	elif beer_id:
		beer = Beer.query.filter(Beer.id == beer_id).first()
		return render_template('beer.html', beer = beer)

	else:
		beers = Beer.query.all()
		return render_template('beer.html', beers = beers, beer = None)

@app.route('/beer/delete/<int:beer_id>', methods = ['GET', 'POST'])
@login_required
def delete_beer(beer_id):
	beer = Beer.query.filter(Beer.id == beer_id).first()
	db.session.delete(beer)
	db.session.commit()

	return redirect(url_for('beer'))
