from flask import redirect, url_for, request, render_template
from model import StockType
from flask.ext.login import login_required
from app import app, db

@app.route('/stocktype', methods = ['GET', 'POST'])
@app.route('/stocktype/<int:stocktype_id>', methods = ['GET'])
@login_required
def stocktype(stocktype_id = None):
	if request.method == 'POST':
		stocktype = StockType(name = request.form['name'], unit = request.form['unit'])
		db.session.add(stocktype)
		db.session.commit()
		return redirect(url_for('stocktype', stocktype_id = stocktype.id))

	elif stocktype_id:
		stocktype = StockType.query.filter(StockType.id == stocktype_id).first()
		return render_template('stocktype.html', stocktype = stocktype)

	else:
		stocktypes = StockType.query.all()
		return render_template('stocktype.html', stocktypes = stocktypes, stocktype = None)

@app.route('/stocktype/delete/<int:stocktype_id>', methods = ['GET', 'POST'])
@login_required
def delete_stocktype(stocktype_id):
	stocktype = StockType.query.filter(StockType.id == stocktype_id).first()
	db.session.delete(stocktype)
	db.session.commit()

	return redirect(url_for('stocktype'))
