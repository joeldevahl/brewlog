from flask import redirect, url_for, request, render_template
from model import Brew
from flask.ext.login import login_required
from app import app, db

@app.route('/brew', methods = ['GET', 'POST'])
@app.route('/brew/<int:brew_id>', methods = ['GET'])
@login_required
def brew(brew_id = None):
	if request.method == 'POST':
		brew = Brew()
		db.session.add(brew)
		db.session.commit()
		return redirect(url_for('brew', brew_id = brew.id))

	elif brew_id:
		brew = Brew.query.filter(Brew.id == brew_id).first()
		return render_template('brew.html', brew = brew)

	else:
		brews = Brew.query.all()
		return render_template('brew.html', brews = brews, brew = None)

@app.route('/brew/delete/<int:brew_id>', methods = ['GET', 'POST'])
@login_required
def delete_brew(brew_id):
	brew = Brew.query.filter(Brew.id == brew_id).first()
	db.session.delete(brew)
	db.session.commit()

	return redirect(url_for('brew'))
