from flask import redirect, url_for
from flask.ext.login import login_required
from app import app
import login
import user
import stocktype
import stockitem
import resource
import delivery
#import brew
import index

@app.route('/')
@login_required
def root():
	return redirect(url_for('index'))
