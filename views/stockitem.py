from flask import redirect, url_for, request, render_template
from model import StockItem, StockType
from flask.ext.login import login_required
from app import app, db

@app.route('/stockitem', methods = ['GET', 'POST'])
@app.route('/stockitem/<int:stockitem_id>', methods = ['GET'])
@login_required
def stockitem(stockitem_id = None):
	if request.method == 'POST':
		stockitem = StockItem(name = request.form['name'], stock_type_id = request.form['stocktype'])
		db.session.add(stockitem)
		db.session.commit()
		return redirect(url_for('stockitem', stockitem_id = stockitem.id))

	elif stockitem_id:
		stockitem = StockItem.query.filter(StockItem.id == stockitem_id).first()
		return render_template('stockitem.html', stockitem = stockitem)

	else:
		stockitems = StockItem.query.all()
		stocktypes = StockType.query.all()
		return render_template('stockitem.html', stockitems = stockitems, stocktypes = stocktypes, stockitem = None)

@app.route('/stockitem/delete/<int:stockitem_id>', methods = ['GET', 'POST'])
@login_required
def delete_stockitem(stockitem_id):
	stockitem = StockItem.query.filter(StockItem.id == stockitem_id).first()
	db.session.delete(stockitem)
	db.session.commit()

	return redirect(url_for('stockitem'))
