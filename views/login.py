from functools import wraps
from hashlib import sha1
from flask import redirect, request, session, url_for, render_template, flash
from model import User
from flask.ext.login import login_user, logout_user
from app import app, login_manager
from forms import LoginForm

@login_manager.user_loader
def load_user(userid):
	user = User.query.filter(User.id == int(userid)).first()
	return user

@app.route('/login', methods = ['GET', 'POST'])
def login():
	error = None
	form = LoginForm()

	if form.validate_on_submit():
		user = User.query.filter(User.username == form.username.data).first()
		if user and user.password == bytearray(sha1(form.password.data).digest()):
			login_user(user)
			flash('You were logged in')
			return redirect(url_for('index'))
		else:
			pass
	else:
		pass

	return render_template('login.html', error = error, form = form)

@app.route('/logout')
def logout():
	logout_user()
	flash('You were logged out')
	return redirect(url_for('login'))
