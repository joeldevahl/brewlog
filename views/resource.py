from flask import redirect, url_for, request, render_template
from model import Event, EventType, Resource, ResourceUsage
from flask.ext.login import login_required, current_user
from app import app, db

def current_usage(res):
	# TODO: rewrite as SQL
	usages = ResourceUsage.query.filter(ResourceUsage.resource_id == res.id).all()
	s = 0
	for u in usages:
		s += u.amount
	return s

def filtered_resources():
	resources = Resource.query.all()
	filtered_resources = []
	# TODO: rewrite as SQL
	for res in resources:
		s = current_usage(res)
		if s < res.amount:
			res.current_amount = res.amount - s
			filtered_resources.append(res)
	return filtered_resources

@app.route('/resource', methods = ['GET', 'POST'])
@login_required
def resource():
	resources = filtered_resources()
	return render_template('resource.html', resources = resources)

@app.route('/resource/waste/<int:resource_id>', methods = ['GET', 'POST'])
@login_required
def waste_resource(resource_id):
	resource = Resource.query.filter(Resource.id == resource_id).one()
	u = current_usage(resource)
	wastage = EventType.query.filter(EventType.name == u"Wastage").one() # TODO: how to get this?
	event = Event(type = wastage, users = [current_user])
	usage = ResourceUsage(event = event, resource = resource, amount = resource.amount - u)
	db.session.add_all([
		event,
		usage
	])
	db.session.commit()
	return redirect(url_for('resource'))
