from flask import render_template, redirect, url_for
from model import Event, EventType, Delivery, Company, StockItem, Resource, ResourceUsage, DELIVERY_DIRECTION_IN, DELIVERY_DIRECTION_OUT
from flask.ext.login import login_required, current_user
from app import app, db
from forms import DeliveryForm, ResourceForm, ResourceUsageForm
from resource import filtered_resources

@app.route('/delivery', methods = ['GET', 'POST'])
@app.route('/delivery/<int:delivery_id>', methods = ['GET', 'POST'])
@login_required
def delivery(delivery_id = None):
	if delivery_id:
		delivery = Delivery.query.filter(Delivery.id == delivery_id).one()
		form = None

		if delivery.direction == DELIVERY_DIRECTION_IN:
			form = ResourceForm()
			form.stock_item.choices = [(c.id, c.name) for c in StockItem.query.all()]

			if form.validate_on_submit():
				stock_item = StockItem.query.filter(StockItem.id == form.stock_item.data).one()
				resource = Resource(item = stock_item, amount = form.amount.data, event = delivery.event)
				db.session.add(resource)
				db.session.commit()
		else:
			form = ResourceUsageForm()
			form.resource.choices = [(r.id, r.item.name) for r in filtered_resources()]
			print form.resource.choices

			if form.validate_on_submit():
				resource = Resource.query.filter(Resource.id == form.resource.data).one()
				usage = ResourceUsage(resource = resource, amount = form.amount.data, event = delivery.event)
				db.session.add(usage)
				db.session.commit()

		if delivery.direction == DELIVERY_DIRECTION_OUT:
			print form.resource.choices
		return render_template('delivery.html', delivery = delivery, form = form)
	else:
		form = DeliveryForm()
		form.company.choices = [(c.id, c.name) for c in Company.query.all()]

		if form.validate_on_submit():
			dt = EventType.query.filter(EventType.name == u"Delivery").one() # TODO: how to get this?
			company = Company.query.filter(Company.id == form.company.data).one()
			event = Event(type = dt, users = [current_user])
			delivery = Delivery(event = event, company = company, direction = form.direction.data)
			db.session.add_all([
				event,
				delivery
			])
			db.session.commit()
			return redirect(url_for('delivery', delivery_id = delivery.id))
		else:
			deliveries = Delivery.query.order_by(Delivery.id.desc()).limit(20).all()
			return render_template('delivery.html', delivery = None, form = form, deliveries = deliveries)
