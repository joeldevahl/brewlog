from flask.ext.wtf import Form
from wtforms import StringField, PasswordField, validators, ValidationError, SelectField
from model import User, DELIVERY_DIRECTION_IN, DELIVERY_DIRECTION_OUT

class LoginForm(Form):
	username = StringField(u'Username', validators = [validators.InputRequired()])
	password = PasswordField(u'Password', validators = [validators.InputRequired()])

class UserForm(Form):
	name = StringField(u'Name', validators = [validators.InputRequired()])
	username = StringField(u'Username', validators = [validators.input_required()])
	password = PasswordField(u'Password', validators = [validators.InputRequired()])

	def validate_username(form, field):
		if User.query.filter(User.username == field.data).count() > 0:
			raise ValidationError('A user with that username already exists')

class DeliveryForm(Form):
	company = SelectField(u'Company', coerce = int)
	direction = SelectField(u'Direction', choices = [(DELIVERY_DIRECTION_IN, u'In'), (DELIVERY_DIRECTION_OUT, u'Out')], coerce = int)

class ResourceForm(Form):
	stock_item = SelectField(u'Stock Item', coerce = int)
	amount = StringField(u'Amount', validators = [validators.InputRequired()])

class ResourceUsageForm(Form):
	resource = SelectField(u'Resource', coerce = int)
	amount = StringField(u'Amount', validators = [validators.InputRequired()])
