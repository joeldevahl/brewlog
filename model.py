from app import db
from hashlib import sha1
from datetime import datetime

class User(db.Model):
	__tablename__ = 'users'
	id = db.Column(db.Integer, db.Sequence('user_id_seq'), primary_key = True)
	name = db.Column(db.Unicode(256), nullable = False)
	username = db.Column(db.String(256), nullable = False)
	password = db.Column(db.Binary(sha1().digest_size), nullable = False)
	active = db.Column(db.Boolean(), default = True)
	admin = db.Column(db.Boolean(), default = False)

	def is_authenticated(self):
		return True

	def is_active(self):
		return self.active

	def is_anonymous(self):
		return False

	def get_id(self):
		return unicode(self.id)

class Company(db.Model):
	__tablename__ = 'companies'
	id = db.Column(db.Integer, db.Sequence('company_id_seq'), primary_key = True)
	name = db.Column(db.Unicode(256), nullable = False)

event_users = db.Table('event_users', db.Model.metadata,
		db.Column('event_id', db.Integer, db.ForeignKey('events.id'), nullable = False),
		db.Column('user_id', db.Integer, db.ForeignKey('users.id'), nullable = False)
	)

class EventType(db.Model):
	__tablename__ = 'event_types'
	id = db.Column(db.Integer, db.Sequence('event_type_id_seq'), primary_key = True)
	name = db.Column(db.Unicode(256), nullable = False)

class StockType(db.Model):
	__tablename__ = 'stock_types'
	id = db.Column(db.Integer, db.Sequence('stock_type_id_seq'), primary_key = True)
	name = db.Column(db.Unicode(256), nullable = False)
	unit = db.Column(db.Unicode(256), nullable = False)

class StockItem(db.Model):
	__tablename__ = 'stock_items'
	id = db.Column(db.Integer, db.Sequence('stock_item_id_seq'), primary_key = True)
	name = db.Column(db.String(256), nullable = False)
	type_id = db.Column(db.Integer, db.ForeignKey('stock_types.id'), nullable = False)
	type = db.relationship('StockType')

class Resource(db.Model):
	__tablename__ = 'resources'
	id = db.Column(db.Integer, db.Sequence('resource_id_seq'), primary_key = True)
	event_id = db.Column(db.Integer, db.ForeignKey('events.id'), nullable = False)
	event = db.relationship('Event')
	item_id = db.Column(db.Integer, db.ForeignKey('stock_items.id'), nullable = False)
	item = db.relationship('StockItem')
	amount = db.Column(db.Integer, nullable = False)

class ResourceUsage(db.Model):
	__tablename__ = 'resource_usages'
	id = db.Column(db.Integer, db.Sequence('resource_usage_id_seq'), primary_key = True)
	event_id = db.Column(db.Integer, db.ForeignKey('events.id'), nullable = False)
	event = db.relationship('Event')
	resource_id = db.Column(db.Integer, db.ForeignKey('resources.id'), nullable = False)
	resource = db.relationship('Resource')
	amount = db.Column(db.Integer, default = 0)

class Event(db.Model):
	__tablename__ = 'events'
	id = db.Column(db.Integer, db.Sequence('event_id_seq'), primary_key = True)
	type_id = db.Column(db.Integer, db.ForeignKey('event_types.id'), nullable = False)
	type = db.relationship('EventType')
	timestamp = db.Column(db.DateTime(), default = datetime.utcnow)
	users = db.relationship('User', secondary = event_users, backref = 'events')
	resources = db.relationship('Resource', backref = 'events')
	resource_usages = db.relationship('ResourceUsage', backref = 'events')

DELIVERY_DIRECTION_IN = 0
DELIVERY_DIRECTION_OUT = 1

class Delivery(db.Model):
	__tablename__ = 'deliveries'
	id = db.Column(db.Integer, db.Sequence('delivery_id_seq'), primary_key = True)
	event_id = db.Column(db.Integer, db.ForeignKey('events.id'), nullable = False)
	event = db.relationship('Event')
	company_id = db.Column(db.Integer, db.ForeignKey('companies.id'), nullable = False)
	company = db.relationship('Company')
	direction = db.Column(db.Integer)
	# TODO: more delivery information

class Brew(db.Model):
	__tablename__ = 'brews'
	id = db.Column(db.Integer, db.Sequence('brew_id_seq'), primary_key = True)
	event_id = db.Column(db.Integer, db.ForeignKey('events.id'), nullable = False)
	event = db.relationship('Event')
	ferment_start = db.Column(db.DateTime())
	ferment_end = db.Column(db.DateTime())
	ferment_loc = db.Column(db.Integer)
	batch_size = db.Column(db.Integer)
	pre_boil_vol = db.Column(db.Integer)
	post_boil_vol = db.Column(db.Integer)
	og = db.Column(db.Integer)
	fg = db.Column(db.Integer)
	# TODO: more brew information

class Bottling(db.Model):
	__tablename__ = 'bottlings'
	id = db.Column(db.Integer, db.Sequence('bottling_id_seq'), primary_key = True)
	event_id = db.Column(db.Integer, db.ForeignKey('events.id'), nullable = False)
	event = db.relationship('Event')
	# TODO: more bottling information

