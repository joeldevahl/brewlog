﻿from app import db

from model import User, Company, Event, EventType, Delivery, Brew, StockType, StockItem, Resource, ResourceUsage

import create_database
from hashlib import sha1

def genpass(pw):
	return bytearray(sha1(pw).digest())

oskar = User(name = u'Oskar de Vahl', username = 'oskar', password = genpass('testpass'))
petter = User(name = u'Petter de Vahl', username = 'petter', password = genpass('testpass'))
joel = User(name = u'Joel de Vahl', username = 'joel', password = genpass('testpass'))
christer = User(name = u'Christer Sehlstedt', username = 'christer', password = genpass('testpass'))

db.session.add_all([
	oskar,
	petter,
	joel,
	christer
])

humlegarden = Company(name = u'Humlegården')

db.session.add_all([
	humlegarden
])

wastage = EventType(name = u'Wastage')
cleaning = EventType(name = u'Cleaning')
delivery = EventType(name = u'Delivery')
brew = EventType(name = u'Brew')
bottling = EventType(name = u'Bottling')

db.session.add_all([
	wastage,
	cleaning,
	delivery,
	brew,
	bottling
])

yeast = StockType(name = u'Yeast', unit = u'g')
malt = StockType(name = u'Malt', unit = u'kg')
hop = StockType(name = u'Hop', unit = u'g')
bottle = StockType(name = u'Bottle', unit = u'st')
cask = StockType(name = u'Cask', unit = u'st')
raw_beer = StockType(name = u'Raw Beer', unit = u'l')
bottled_beer = StockType(name = u'Bottled Beer', unit = u'st')
cask_beer = StockType(name = u'Cask Beer', unit = u'st')

db.session.add_all([
	yeast,
	malt,
	hop,
	bottle,
	raw_beer,
	bottled_beer,
	cask_beer,
])

pale_ale_malt = StockItem(name = u'Pale Ale', type = malt)
pilsner_malt = StockItem(name = u'Pilsner', type = malt)
raw_bitter = StockItem(name = u'Bitter Länsman', type = raw_beer)
bottled_bitter = StockItem(name = u'Bitter Länsman', type = bottled_beer)
cask_bitter = StockItem(name = u'Bitter Länsman', type = cask_beer)

db.session.add_all([
	pale_ale_malt,
	pilsner_malt,
	raw_bitter,
	bottled_bitter,
	cask_bitter
])

delivery1_event = Event(type = delivery, users = [joel])
delivery1 = Delivery(event = delivery1_event, company = humlegarden)
delivery1_resource1 = Resource(event = delivery1_event, item = pilsner_malt, amount = 12)
delivery1_resource2 = Resource(event = delivery1_event, item = pale_ale_malt, amount = 32)

db.session.add_all([
	delivery1_event,
	delivery1,
	delivery1_resource1,
	delivery1_resource2
])

brew1_event = Event(type = brew, users = [joel])
brew1 = Brew(event = delivery1_event)
brew1_usage1 = ResourceUsage(event = brew1_event, resource = delivery1_resource1, amount = 12)
brew1_usage2 = ResourceUsage(event = brew1_event, resource = delivery1_resource2, amount = 4)
brew1_resource1 = Resource(event = brew1_event, item = raw_bitter, amount = 1000)

db.session.add_all([
	brew1_event,
	brew1,
	brew1_usage1,
	brew1_usage2,
	brew1_resource1
])

db.session.commit()
